from airflow import DAG

from airflow.utils.dates import days_ago
from airflow.operators.bash import BashOperator

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['ricsue@amazon.com'],
    'email_on_failure': False,
    'email_on_retry': False
}

DAG_ID = "testing_dag"

dag = DAG(
    dag_id=DAG_ID,
    default_args=default_args,
    description='General Purpose Testing DAG',
    schedule_interval=None,
    start_date=days_ago(2),
    catchup=False,
    tags=['aws','demo'],
)

debug = BashOperator(
        task_id='debug_env',
        bash_command="env && pip list",
        dag=dag
    )
debug
